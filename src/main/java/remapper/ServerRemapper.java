package remapper;

import net.md_5.specialsource.JarMapping;
import net.md_5.specialsource.JarRemapper;

public class ServerRemapper extends JarRemapper {

	public ServerRemapper(JarMapping jarMapping) {
		super(jarMapping);
	}

	public String mapSignature(String signature, boolean typeSignature) {
		try {
			return super.mapSignature(signature, typeSignature);
		} catch (Exception e) {
			return signature;
		}
	}
}
