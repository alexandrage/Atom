package org.bukkit.plugin.java;

import com.google.common.io.ByteStreams;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.CodeSigner;
import java.security.CodeSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.apache.commons.lang3.Validate;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.PluginDescriptionFile;

import net.md_5.specialsource.JarMapping;
import net.md_5.specialsource.JarRemapper;
import net.md_5.specialsource.provider.ClassLoaderProvider;
import net.md_5.specialsource.provider.JointProvider;
import net.md_5.specialsource.repo.RuntimeRepo;
import net.minecraft.server.MinecraftServer;
import remapper.ClassInheritanceProvider;
import remapper.MappingLoader;
import remapper.ReflectionTransformer;
import remapper.Server;
import remapper.ServerRemapper;

/**
 * A ClassLoader for plugins, to allow shared classes across multiple plugins
 */
final class PluginClassLoader extends URLClassLoader {
	private final JavaPluginLoader loader;
	private final Map<String, Class<?>> classes = new java.util.concurrent.ConcurrentHashMap<String, Class<?>>(); // Spigot
	private final PluginDescriptionFile description;
	private final File dataFolder;
	private final File file;
	private final JarFile jar;
	private final Manifest manifest;
	private final URL url;
	final JavaPlugin plugin;
	private JavaPlugin pluginInit;
	private IllegalStateException pluginState;

	// Remap Start
	private JarRemapper remapper;
	private JarMapping jarMapping;
	// Remap Start

	// Spigot Start
	static {
		// Remap Start
		ReflectionTransformer.init();
		// Remap end
		try {
			java.lang.reflect.Method method = ClassLoader.class.getDeclaredMethod("registerAsParallelCapable");
			if (method != null) {
				boolean oldAccessible = method.isAccessible();
				method.setAccessible(true);
				method.invoke(null);
				method.setAccessible(oldAccessible);
				org.bukkit.Bukkit.getLogger().log(java.util.logging.Level.INFO,
						"Set PluginClassLoader as parallel capable");
			}
		} catch (NoSuchMethodException ex) {
			// Ignore
		} catch (Exception ex) {
			org.bukkit.Bukkit.getLogger().log(java.util.logging.Level.WARNING,
					"Error setting PluginClassLoader as parallel capable", ex);
		}
	}
	// Spigot End

	PluginClassLoader(final JavaPluginLoader loader, final ClassLoader parent, final PluginDescriptionFile description,
			final File dataFolder, final File file) throws IOException, InvalidPluginException, MalformedURLException {
		super(new URL[] { file.toURI().toURL() }, parent);
		Validate.notNull(loader, "Loader cannot be null");

		this.loader = loader;
		this.description = description;
		this.dataFolder = dataFolder;
		this.file = file;
		this.jar = new JarFile(file);
		this.manifest = jar.getManifest();
		this.url = file.toURI().toURL();

		// Remap Start
		jarMapping = MappingLoader.loadMapping();
		JointProvider provider = new JointProvider();
		provider.add(new ClassInheritanceProvider());
		provider.add(new ClassLoaderProvider(this));
		jarMapping.setFallbackInheritanceProvider(provider);
		remapper = new ServerRemapper(jarMapping);
		// Remap end

		try {
			Class<?> jarClass;
			try {
				jarClass = Class.forName(description.getMain(), true, this);
			} catch (ClassNotFoundException ex) {
				throw new InvalidPluginException("Cannot find main class `" + description.getMain() + "'", ex);
			}

			Class<? extends JavaPlugin> pluginClass;
			try {
				pluginClass = jarClass.asSubclass(JavaPlugin.class);
			} catch (ClassCastException ex) {
				throw new InvalidPluginException(
						"main class `" + description.getMain() + "' does not extend JavaPlugin", ex);
			}

			plugin = pluginClass.newInstance();
		} catch (IllegalAccessException ex) {
			throw new InvalidPluginException("No public constructor", ex);
		} catch (InstantiationException ex) {
			throw new InvalidPluginException("Abnormal plugin type", ex);
		}
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		return findClass(name, true);
	}

	Class<?> findClass(String name, boolean checkGlobal) throws ClassNotFoundException {
		if (name.startsWith("org.bukkit.") || name.startsWith("net.minecraft.")) {
			throw new ClassNotFoundException(name);
		}
		Class<?> result = classes.get(name);

		// Remap Start
		synchronized (name.intern()) {
			if (result == null) {
				if (checkGlobal) {
					result = loader.getClassByName(name);
				}

				if (result == null) {
					result = remappedFindClass(name);

					if (result != null) {
						loader.setClass(name, result);
					}
				}

				if (result == null) {
					throw new ClassNotFoundException(name);
				}

				classes.put(name, result);
			}
		}
		// Remap end

		return result;
	}

	@Override
	public void close() throws IOException {
		try {
			super.close();
		} finally {
			jar.close();
		}
	}

	Set<String> getClasses() {
		return classes.keySet();
	}

	synchronized void initialize(JavaPlugin javaPlugin) {
		Validate.notNull(javaPlugin, "Initializing plugin cannot be null");
		Validate.isTrue(javaPlugin.getClass().getClassLoader() == this,
				"Cannot initialize plugin outside of this class loader");
		if (this.plugin != null || this.pluginInit != null) {
			throw new IllegalArgumentException("Plugin already initialized!", pluginState);
		}

		pluginState = new IllegalStateException("Initial initialization");
		this.pluginInit = javaPlugin;

		javaPlugin.init(loader, loader.server, description, dataFolder, file, this);
	}

	// Remap Start
	private Class<?> remappedFindClass(String name) throws ClassNotFoundException {
		Class<?> result = null;

		try {
			// Load the resource to the name
			String path = name.replace('.', '/').concat(".class");
			URL url = this.findResource(path);
			if (url != null) {
				InputStream stream = url.openStream();
				if (stream != null) {
					byte[] bytecode = null;

					// Remap the classes
					bytecode = remapper.remapClassFile(stream, RuntimeRepo.getInstance());
					bytecode = ReflectionTransformer.transform(bytecode);

					// Define (create) the class using the modified byte code
					// The top-child class loader is used for this to prevent
					// access violations
					// Set the codesource to the jar, not within the jar, for
					// compatibility with
					// plugins that do new
					// File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI()))
					// instead of using getResourceAsStream - see
					// https://github.com/MinecraftPortCentral/Cauldron-Plus/issues/75
					JarURLConnection jarURLConnection = (JarURLConnection) url.openConnection(); // parses
																									// only
					URL jarURL = jarURLConnection.getJarFileURL();
					CodeSource codeSource = new CodeSource(jarURL, new CodeSigner[0]);

					result = this.defineClass(name, bytecode, 0, bytecode.length, codeSource);
					if (result != null) {
						// Resolve it - sets the class loader of the class
						this.resolveClass(result);
					}
				}
			}
		} catch (Throwable t) {
			throw new ClassNotFoundException("Failed to remap class " + name, t);
		}

		return result;
	}

	// Server - can't get the package because no class below the package,
	// remap it
	protected Package getPackage(String name) {
		if (name == "org.bukkit.craftbukkit")
			name = "org.bukkit.craftbukkit." + Server.getNativeVersion();
		return super.getPackage(name);
	}
	// Remap End
}